#!/bin/bash

SESSION='emprego'
PORT=6008

# start a new session
tmux new-session -d -s $SESSION -n editor
tmux new-window -t $SESSION:2 -n server
tmux new-window -t $SESSION:3 -n console
tmux new-window -t $SESSION:4 -n console

# Execute commands
tmux send-keys -t $SESSION:1 'vim' enter
tmux send-keys -t $SESSION:2 "rails server -b 0.0.0.0 -p $PORT" enter
tmux send-keys -t $SESSION:4 "guard -P livereload" enter

tmux attach -t $SESSION:editor
