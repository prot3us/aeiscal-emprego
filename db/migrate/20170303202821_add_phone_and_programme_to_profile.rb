class AddPhoneAndProgrammeToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :phone, :string
    add_reference :profiles, :programme, foreign_key: true
  end
end
