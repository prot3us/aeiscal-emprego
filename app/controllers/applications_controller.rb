class ApplicationsController < ApplicationController
  def create
    @user = current_user
    @application = Application.new(application_params)
    @application.user = @user
    if @application.save
      redirect_to :back, notice: "Candidatura com sucesso"
    else
      redirect_to :back, notice: "Candidatura sem sucesso"
    end
  end

  def destroy
    @application = Application.find params[:id]
    @application.destroy
    redirect_to :back
  end


  private
    def application_params
      params.require(:application).permit(:job_offer_id)
    end
end
