class JobOfferMailer < ApplicationMailer

  def applications_link(job_offer)
    @job_offer = job_offer
    mail to: @job_offer.email, subject: 'Candidaturas'
  end

end
