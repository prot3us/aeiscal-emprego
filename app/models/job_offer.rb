class JobOffer < ApplicationRecord
  has_many :applications, dependent: :destroy
  has_many :views, foreign_key: :job_offer_id, class_name: "JobView", dependent: :destroy
  has_and_belongs_to_many :programmes

  has_attached_file :image, styles: { medium: "300x300>" }, default_url: "/images/missing.jpg"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  validate :expires_at_validation

  has_many :users, through: :applications

  before_create :generate_random_id

  before_validation on: [:create] do
    self.expires_at ||= Date.today
  end

  def expired?
    Date.today > expires_at
  end

  def expires_at_validation
    if expires_at > JobOffer.max_expiration_date
      errors.add(:expires_at, "tem prazo maximo de 3 meses")
    end
  end

  def generate_random_id
    while(!JobOffer.find_by(random_id: (self.random_id = SecureRandom.hex)).blank?)
    end
  end

  def self.max_expiration_date
    Date.today + 3.months
  end

  def self.locations
    [
      'Aveiro',
      'Beja',
      'Braga',
      'Bragança',
      'Castelo Branco',
      'Coimbra',
      'Évora',
      'Faro',
      'Guarda',
      'Leiria',
      'Lisboa',
      'Portalegre',
      'Porto',
      'Santarém',
      'Setúbal',
      'Viana do Castelo',
      'Vila Real',
      'Viseu',
      'Madeira',
      'Açores'
    ]
  end

  def self.types_of_contract
    ['Efetivo', 'Estágio', 'Part-time', 'Outro']
  end

  def update_view_count
    self.view_count = self.views.count
    self.save
  end

  def update_applications_number
    self.number_of_applications = self.applications.count
    self.save
  end
end
