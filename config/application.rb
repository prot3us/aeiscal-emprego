require_relative 'boot'

require 'rails/all'
require "paperclip/storage/ftp"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AeiscalEmprego
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.i18n.enforce_available_locales = false
    config.i18n.available_locales = [:pt]
    config.i18n.default_locale = :pt
    config.action_dispatch.default_headers = {
      'Access-Control-Allow-Origin' => 'http://ficheiros.aeiscal.pt',
      'Access-Control-Request-Method' => %w{GET POST OPTIONS}.join(",")
    }
  end
end
