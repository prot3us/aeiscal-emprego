class CreateJobOffersAndProgrammes < ActiveRecord::Migration[5.0]
  def change
    create_table :job_offers_programmes, id: false do |t|
      t.references :job_offer, foreign_key: true
      t.references :programme, foreign_key: true
    end
    add_index :job_offers_programmes, [:job_offer_id, :programme_id]
  end
end
