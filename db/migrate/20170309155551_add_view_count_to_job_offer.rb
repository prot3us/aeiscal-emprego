class AddViewCountToJobOffer < ActiveRecord::Migration[5.0]
  def change
    add_column :job_offers, :view_count, :integer, default: 0
  end
end
