class Degree < ApplicationRecord
  has_many :programmes, dependent: :destroy
end
