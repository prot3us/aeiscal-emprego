class AddNumberOfYearsToProgramme < ActiveRecord::Migration[5.0]
  def change
    add_column :programmes, :number_of_years, :integer
  end
end
