class AddOrderToDegree < ActiveRecord::Migration[5.0]
  def change
    add_column :degrees, :order, :integer
  end
end
