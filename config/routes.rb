Rails.application.routes.draw do
  resources :programmes
  resources :degrees
  resources :applications, only: [:create, :show, :destroy]

  resources :job_offers, path: '/emprego' do
    collection do
      get 'estudantes/:random_id' => 'job_offers#students', as: :students
    end
  end

  devise_for :users
  resources :users do
    collection do
      resources :profiles
    end
  end

  match 'perfil' => 'profiles#show', as: :my_profile, via: :get
  match 'perfil/editar' => 'profiles#edit', as: :edit_my_profile, via: :get
  match 'utilizador' => 'users#show', as: :my, via: :get

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #
  root 'pages#index'
end
