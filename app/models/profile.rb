class Profile < ApplicationRecord

  belongs_to :user
  belongs_to :programme
  validates_presence_of :name, :user

  after_create :make_student

  has_attached_file :curriculum, {
    storage: :ftp,
    path: ENV['ftp_path'] + "/curriculum/:id/:filename",
    url: ENV['http_path'] + "/curriculum/:id/:filename",
    ftp_servers: [
      {
        host: ENV['ftp_host'],
        user: ENV['ftp_user'],
        password: ENV['ftp_password'],
        passive: true
      }
    ]
  }

  validates_attachment_content_type :curriculum, content_type: "application/pdf"

  def make_student
    self.user.make_student
  end

  def email
    self.user.email
  end

  def degree
    self.programme.degree.name
  end
end
