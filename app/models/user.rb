class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :confirmable, :recoverable, :rememberable, :trackable, :validatable
  
  after_create :assign_default_role

  has_one :profile, dependent: :destroy
  has_many :applications, dependent: :destroy
  has_many :views, foreign_key: :user_id, class_name: "JobView", dependent: :destroy

  accepts_nested_attributes_for :applications

  def assign_default_role
    self.add_role :newuser if self.roles.blank?
  end

  def assign_role new_role
    self.remove_role :newuser
    self.add_role new_role
  end

  def make_student
    self.assign_role :student
  end

  def name
    if self.profile.blank?
      return self.email
    else
      return self.profile.name
    end
  end

  def create_view job_offer
    if self.has_role? :student
      JobView.create_later(self, job_offer)
    end
  end

end
