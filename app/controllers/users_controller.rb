class UsersController < ApplicationController
  load_and_authorize_resource except: :show
  before_action :set_user, only: [:show, :update]

  def index
    role = Role.find_by(name: 'student')
    if role
      @users = role.users
    else
      @users = {}
    end
    render layout: 'admin'
  end

  def show
    authorize! :manage, @user

    if @user == current_user
      @profile = @user.profile
    end

    if can? :manage, User
      render :admin_show
    end
  end

  def update
    if @user.update(user_params)
      redirect_to job_offers_path, notice: 'User atualizado'
    else
      redirect_to job_offers_path, notice: 'Erro a atualizar utilizador'
    end
  end

  private
    def user_params
      params.require(:user).permit(application: [:user_id, :job_offer_id])
    end

    def set_user
      if params[:id].blank?
        @user = current_user
      else
        @user = User.find params[:id]
      end
    end

end
