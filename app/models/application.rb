class Application < ApplicationRecord
  belongs_to :user
  belongs_to :job_offer

  validates_presence_of :user, :job_offer

  validates :job_offer, uniqueness: { scope: :user }

  after_create :update_number_of_applications
  after_destroy :update_number_of_applications

  accepts_nested_attributes_for :job_offer

  def update_number_of_applications
    self.job_offer.update_applications_number
  end
end
