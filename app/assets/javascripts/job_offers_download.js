$(document).on('turbolinks:load', function() {
  var $download_button = $('#job-offer-download-all');
  var $students = $('.job-offer-student');

  if($download_button.length > 0 && $students.length > 0) {
    var zipFileName = 'cvs.zip';
    var zip = new JSZip();
    var downloaded = 0;

    $download_button.click(function(e) {
      e.preventDefault();
      $.each($students, function(index, student) {
        var $link = $(student).find('a');
        var url = $link[0].href;
        var fileName = $link.data('filename');

        JSZipUtils.getBinaryContent(url, function(err, data) {
          if(err) {
            throw err;
          }
          zip.file(fileName, data, {binary: true});
          downloaded++;

          if(downloaded == $students.length) {
            zip.generateAsync({type: "blob"}).then(function(zipFile) {
              saveAs(zipFile, zipFileName);
            });
          }
        });
      })
    });
  }

});
