class JobOffersController < ApplicationController
  before_action :set_job_offer, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: [:students]
  layout 'admin'

  # GET /job_offers
  # GET /job_offers.json
  def index
    if can? :manage, JobOffer
      if params[:expired]
        @job_offers = JobOffer.where(expires_at: (Date.today - 1.year)..(Date.today - 1.day)).order(approved: :asc, expires_at: :asc)
      else
        @job_offers = JobOffer.where(expires_at: Date.today..(Date.today + 3.months)).or(JobOffer.where(approved: false)).order(approved: :asc, expires_at: :asc)
      end
      render :admin_index
    else
      @filters = {}
      if params[:programme]
        @filters[:programme] = Programme.find(params[:programme]).full_name
        @job_offers = JobOffer.left_joins(:programmes).where(search_params).order(order_params)
      else
        @job_offers = JobOffer.where(search_params).order(order_params)
      end
      if params[:location]
        @filters[:location] = params[:location]
      end
      if params[:contract]
        @filters[:contract] = params[:contract]
      end
      render layout: 'job_offer'
    end
  end

  # GET /job_offers/1
  # GET /job_offers/1.json
  def show
    current_user.create_view @job_offer
  end

  # GET /job_offers/new
  def new
    @job_offer = JobOffer.new expires_at: 3.months.from_now
    render layout: 'application'
  end

  # GET /job_offers/1/edit
  def edit
  end

  # POST /job_offers
  # POST /job_offers.json
  def create
    @job_offer = JobOffer.new(job_offer_params)

    respond_to do |format|
      if @job_offer.save
        message = 'Oferta de emprego criada. Aguarda aprovação por parte da AEISCAL' 
        if can? :read, @job_offer
          format.html { redirect_to @job_offer, notice: message }
        else
          format.html { redirect_to root_path, notice: message }
        end
        format.json { render :show, status: :created, location: @job_offer }
      else
        format.html { render :new }
        format.json { render json: @job_offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_offers/1
  # PATCH/PUT /job_offers/1.json
  def update
    respond_to do |format|
      if @job_offer.update(job_offer_params)
        if job_offer_params[:applications_sent]
          JobOfferMailer.applications_link(@job_offer).deliver_later
        end
        format.html { redirect_to @job_offer, notice: 'Job offer was successfully updated.' }
        format.json { render :show, status: :ok, location: @job_offer }
      else
        format.html { render :edit }
        format.json { render json: @job_offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_offers/1
  # DELETE /job_offers/1.json
  def destroy
    @job_offer.destroy
    respond_to do |format|
      format.html { redirect_to job_offers_url, notice: 'Job offer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def students
    @job_offer = JobOffer.find_by random_id: params[:random_id]
    @students = @job_offer.users
    render layout: 'application'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_offer
      @job_offer = JobOffer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_offer_params
      params.require(:job_offer).permit(:applications_sent, :image, :approved, :company, :location, :minimum_requirements, :type_of_contract, :contract_duration, :name, :email, :phone, :start_time, :end_time, :expires_at, :description, :position, :salary, programme_ids: [])
    end

    def search_params
      search_params = params.permit(:programme, :approved, :location, :contract, :filter)
      search = {expires_at: Date.today..(Date.today + 3.months)}
      if search_params[:contract]
        search[:type_of_contract] = search_params[:contract]
      end
      if search_params[:programme]
        search['job_offers_programmes.programme_id'] = search_params[:programme]
      end
      if search_params[:location]
        search[:location] = search_params[:location]
      end
      if search_params[:filter] == 'applications'
        search[:number_of_applications] = 0
      elsif search_params[:filter] == 'expiring'
        search[:expires_at] = Date.today..(Date.today + 2.weeks)
      end
      search[:approved] = true
      search
    end

    def order_params
      order_param = params[:order]
      order = {}
      if order_param == 'applications'
        order = {number_of_applications: :desc}
      elsif order_param == 'views'
        order = {view_count: :desc}
      end
      if params[:filter] == 'expiring'
        order[:expires_at] = :asc
      end
      order[:updated_at] = :desc
      order
    end
end
