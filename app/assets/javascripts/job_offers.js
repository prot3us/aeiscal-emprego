$(document).on('turbolinks:load', function() {
  var $panel = $('.job-offer-panel');
  var closeButton = $('#job-offer-close');

  $('.job-link').click(function(e) {
    e.preventDefault();
    var url = this.href;
    var id = $(this).data('id');

    $.getJSON(url, function(job) {
      $('#job-company').html(job.company);
      $('#job-position').html(job.position);
      $('#job-location').html(job.location);
      $('#job-expires-at').html(job.expires_at);
      var job_requirements = $('#job-requirements');
      for(var programme in job.requirements) {
        job_requirements.html(job_requirements.html() + job.requirements[programme].name + '</br>');
      }
      $('#job-minimum-requirements').html(job.minimum_requirements);
      $('#job-contract').html(job.minimum_requirements);
      $('#job-duration').html(job.contract_duration);
      $('#job-start').html(job.start_time);
      $('#job-end').html(job.end_time);
      $('#job-description').html(job.description);
      $('#job-salary').html(job.salary);
      $('#application_job_offer_id').val(job.id);
      $('#job-img').attr('src', job.image);
      $applicationLink = $('#job-offer-link');
      $newApplicationButton = $('#new_application .button');
      if(job.applied) {
        $newApplicationButton.addClass('hide');
        $applicationLink.removeClass('hide');
        $applicationTrueLink = $('#application' + id);
        $applicationLink.click(function(e) {
          e.preventDefault();
          $applicationTrueLink.click();
        })
      } else {
        $newApplicationButton.removeClass('hide');
        $applicationLink.addClass('hide');
      }
      $('body').css('overflow', 'hidden');
      openPanel();
    });


    $panel.click(closePanel);
    closeButton.click(closePanel);

    $panel.children().click(function(e) {
      e.stopPropagation();
    });

  });

  function closePanel() {
    $panel.removeClass('active');
    $panel.one('transitionend', function() {
      $panel.addClass('hide');
    });
    $('body').css('overflow', '');
  }

  function openPanel() {
    $panel.removeClass('hide');
    setTimeout(function() {
      $panel.addClass('active');
    }, 20);
  }

  function resizeJobs() {
    var jobs = $('.jobs');
    jobs.width('auto');
    var link = jobs.children().first();
    var nr_of_children = jobs.children().length;
    var width = (jobs.width());
    var elements_width = link.width();
    elements = Math.floor(width / elements_width);
    jobs.width(elements * elements_width);
  }

  function showPanel() {
  }

  resizeJobs();

  $(window).on('resize', resizeJobs);

});
