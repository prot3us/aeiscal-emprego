class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@emprego.aeiscal.pt'
  layout 'mailer'
end
