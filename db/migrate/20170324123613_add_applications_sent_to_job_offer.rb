class AddApplicationsSentToJobOffer < ActiveRecord::Migration[5.0]
  def change
    add_column :job_offers, :applications_sent, :boolean, default: false
  end
end
