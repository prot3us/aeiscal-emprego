
$(document).on('turbolinks:load', function() {
  Dropzone.autoDiscover = false;
  var link = $('#curriculum-url, #curriculum-missing');
  var linkMissing = $('#curriculum-missing');
  var button = $('#curriculum-upload-button');
  var destroyButton = $('.curriculum-destroy');
  var text = "";
  $("#curriculum-upload-form").dropzone({
    paramName: "profile[curriculum]",
    clickable: "#curriculum-upload-button",
    parallelUploads: 1,
    maxFilesize: 5,
    previewTemplate: "<div></div>",
    init: function() {

      // PROCESSING
      this.on('processing', function(file) {
        link.attr('href', '#');
        link.addClass('orange');
        button.addClass('orange');
        console.log('processing');
        text = button.html();
        button.html('A enviar');
        destroyButton.addClass('hidden');
      });

      // PROGRESS
      this.on('uploadprogress', function(file, progress, bytesSent) {
        button.html('A enviar: ' + Math.round(progress) + '%');
        if(progress == 100) {
          link.removeClass('orange');
          button.removeClass('orange');
          link.addClass('yellow');
          button.addClass('yellow');
          button.html('A processar');
        }
      });

      // SUCCESS
      this.on('success', function(file, response) {
        link.attr('href', response.curriculum_url);
        console.log('done');
        link.removeClass('yellow');
        button.removeClass('yellow');
        destroyButton.removeClass('hidden');
        button.html(text);
        link.removeClass('hidden');
        linkMissing.addClass('hidden');
      });
    }
  });
});
