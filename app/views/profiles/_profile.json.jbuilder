json.extract! profile, :id, :name, :user_id, :created_at, :updated_at
json.url profile_url(profile, format: :json)
json.curriculum_url profile.curriculum.url
