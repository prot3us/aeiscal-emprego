class RemoveNumberOfYearsFromProgramme < ActiveRecord::Migration[5.0]
  def change
    remove_column :programmes, :number_of_years, :integer
  end
end
