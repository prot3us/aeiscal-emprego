class PagesController < ApplicationController

  def index
    if user_signed_in?
      if current_user.has_role? :admin
        redirect_to users_path
      elsif current_user.has_role? :newuser
        redirect_to new_profile_path
      else
        redirect_to job_offers_path
      end
    else
      render layout: 'no-padding'
    end
  end

end
