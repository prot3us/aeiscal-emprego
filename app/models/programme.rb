class Programme < ApplicationRecord
  belongs_to :degree
  has_and_belongs_to_many :job_offers
  has_many :profiles

  def full_name
    "[#{degree.name}] #{name}"
  end
end
