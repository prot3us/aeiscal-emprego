json.extract! job_offer, :id, :company, :end_time, :description, :position, :created_at, :updated_at, :location, :minimum_requirements, :type_of_contract, :contract_duration
json.expires_at l job_offer.expires_at
json.requirements job_offer.programmes
json.url job_offer_url(job_offer, format: :json)
json.start_time job_offer.start_time.strftime("%H:%M")
json.end_time job_offer.end_time.strftime("%H:%M")
json.salary number_to_currency job_offer.salary
json.image job_offer.image.url
json.applied job_offer.users.exists? current_user
if job_offer.users.exists?(current_user)
  json.application_url application_url(job_offer.applications.find_by user: current_user)
end
