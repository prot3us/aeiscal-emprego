

$(document).on('turbolinks:load', function() {
  var menu = $('.menu>li');
  menu.each(function() {
    var jthis = $(this);
    var close;
    jthis.hover(function(e) {
      $(this).find('.submenu').addClass('active');
      clearTimeout(close);
    }, function(e) {
      var submenu = $(this).find('.submenu');
      close = setTimeout(function() {
        submenu.removeClass('active');
      }, 300);
    });
  });
})
