class AddExpiresAtToJobOffer < ActiveRecord::Migration[5.0]
  def change
    add_column :job_offers, :expires_at, :date
  end
end
