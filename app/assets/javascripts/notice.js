$(document).on('turbolinks:load', function () {
  var $notice = $("#notice");
  var vanishTimeout;

  vanishDelay();

  $notice.click(function() {
    vanish();
  });

  $notice.hover(
    function() {
      cancelVanish();
    },
    function() {
      vanishDelay();
    }
  );

  function vanishDelay() {
    vanishTimeout = setTimeout(function() {
      vanish();
    }, 5000);
  }

  function cancelVanish() {
    $notice.removeClass('vanish');
    clearTimeout(vanishTimeout);
  }

  function vanish() {
    $notice.addClass('vanish');
    $notice.one('transitionend', function() {
      $notice.remove();
    });
  }
});
